#!/bin/sh
# File: gallery/toggle-editing.cgi
# Author: bgstack15@gmail.com
# Startdate: 2021-01-25 11:49
# SPDX-License-Identifier: GPL-3.0
# Title: Toggle the editing of this gallery
# Purpose: toggle the [edit] buttons in the requested gallery
# History:
# Usage:
#    called by a link in the custom sigal theme
#    required parameters include id
# Reference:
#    apply.cgi
# Improve:
# Documentation:
# Dependencies:
#    /etc/gallery-cgi.conf
#    sigal.conf.py from indicated gallery_id
# Reverse dependencies:
DEBUG=0
export DRYRUN=
export meta="<meta" # for turning off the automatic reloads
response=0
test -e /etc/gallery-cgi.conf && . /etc/gallery-cgi.conf
# for the "sed -i"-ed conf file:
chgrp_string="admins"
chmod_string="g+rw"

fix_perms() {
   # call: fix_perms "${file}"
   chmod "${chmod_string}" "${1}"
   chgrp "${chgrp_string}" "${1}"
}

printf "%s\n\n" "Content-type: text/html"
echo "<html><head><title>Toggle editing</title>"
echo "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
echo "</head><body>"

# find gallery_id from query string, for GET
echo "${QUERY_STRING}" | grep -qE "(^|\&|\?)id=[^\&]+" && {
   export gallery_id="$( echo "${QUERY_STRING}" | grep -oE "(^|\&|\?)id=[^\&]+" | awk -F'=' '{$1="";print;}' | sed -r -e 's:^ ::' )"
}

# dynamically load the config file from the gallery_id defined in gallery-cgi.conf
if test "${REQUEST_METHOD}" = "GET" ;
then
   eval export sigal_conf_py=\"\${"${gallery_id}"}\"/sigal.conf.py
fi

test -n "${DEBUG}" && test ${DEBUG} -ge 3 1>/dev/null 2>&1 && {
   echo "<pre>"
   env
   echo "</pre>"
}

if test "${REQUEST_METHOD}" = "POST" ;
then
   # if POST, then gallery_id is here and not earlier.

   stdin="$( cat )"

   # interpret input variables
   export boundary="$( echo "${CONTENT_TYPE}" | awk '{print $2}' | awk -F'=' '{print $2}' )"
   export stdin2="$( echo "${stdin}" | sed -r -e "s/-*${boundary}-*/%%%%%%%%%%/g;" | awk '/%%%%%%%/{a=a+1} $0 !~ /%%%%%/ && a {print a,$0}' )"

   # find gallery_id from form data
   export gallery_id="$( echo "${stdin2}" | awk '/Content-Disposition.*="id"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' | tr -d '[\n\r]' )"
   export referer="$( echo "${stdin2}" | awk '/Content-Disposition.*="referer"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' | tr -d '[\n\r]' )"
   test -n "${DEBUG}" && test ${DEBUG} -ge 2 1>/dev/null 2>&1 && {
      echo "<pre>"
      echo "gallery_id=${gallery_id}"
      echo "STDIN START"
      echo "${stdin2}"
      echo "STDIN STOP"
      echo "</pre>"
   }
   export password="$( echo "${stdin2}" | awk '/Content-Disposition.*="password"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{$1="";print;}' | sed -r -e 's/^\s*//;' | tr -d '[\n\r]' )"
   test -n "${DEBUG}" && test ${DEBUG} -ge 2 1>/dev/null 2>&1 && {
      echo "<pre>"
      echo "password=\"${password}\""
      echo "desired_password=\"${desired_password}\""
      echo "</pre>"
   }

   # dynamically load the config file from the POST gallery_id defined in gallery-cgi.conf
   eval export sigal_conf_py=\"\${"${gallery_id}"}\"/sigal.conf.py
   # read config file for desired values
   desired_password="$( awk -F'=' '$1 ~ /edit_password/ {print $2}' "${sigal_conf_py}" | sed -r -e "s/[\"']//g;" -e 's/^ //;' | tr '[A-Z]' '[a-z]' )"
   # check if password is correct
   if test "${password}" = "${desired_password}" ;
   then
      action="enable"
   else
      # bad password
      echo "bad password"
      echo "${meta} http-equiv=\"Refresh\" content=\"0; url='${referer}'\" />"
   fi


else
   # HANDLE the GET
   edit_enabled="$( awk -F'=' '$1 ~ /edit_enabled/ {print $2}' "${sigal_conf_py}" | sed -r -e "s/[\"']//g;" -e 's/^ //;' | tr '[A-Z]' '[a-z]' )"
   if test "${edit_enabled}" = "true" || test "${edit_enabled}" = "1" || test "${edit_enabled}" = "yes" || test "${edit_enabled}" = "on" ;
   then
      # go ahead and disable it
      action="disable"
   else
      # we need to ask to enable it
      echo "<form method=\"post\" enctype=\"multipart/form-data\" id=\"toggle\" action=\"$( basename "${0}" )\" >"
      echo "   <label for=\"password\">Password:</label>"
      echo "   <input type=\"password\" id=\"password\" name=\"password\">"
      echo "   <input type=\"hidden\" id=\"referer\" name=\"referer\" value=\"${HTTP_REFERER}\">"
      echo "   <input type=\"hidden\" id=\"id\" name=\"id\" value=\"${gallery_id}\">"
      echo "   <input type=\"submit\" value=\"Submit\">"
      echo "</form>"
   fi
fi

# take action if necessary
case "${action}" in
   disable)
      echo "Disabling editing..."
      sed -i -r -e '/edit_enabled/{s/True/False/}' "${sigal_conf_py}" 1>&2
      response=$?
      fix_perms "${sigal_conf_py}"
      test ${response} -eq 0 && echo "${meta} http-equiv=\"Refresh\" content=\"1; url='${HTTP_REFERER}'\" />"
      ;;
   enable)
      echo "Enabling editing..."
      sed -i -r -e '/edit_enabled/{s/False/True/}' "${sigal_conf_py}" 1>&2
      response=$?
      fix_perms "${sigal_conf_py}"
      test ${response} -eq 0 && echo "${meta} http-equiv=\"Refresh\" content=\"1; url='${referer}'\" />"
      ;;
   "")
      :
      ;;
   *)
      echo "Unknown action ${action}."
      ;;
esac

printf '%s' "</body></html>"
exit ${response}
