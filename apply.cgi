#!/bin/sh
# File: /mnt/public/www/cgi-bin/gallery/apply.cgi
# Author: bgstack15@gmail.com
# Startdate: 2021-01-24 22:12
# SPDX-License-Identifier: GPL-3.0
# Title: Apply Changes to Disk
# Package: gallery
# Purpose: apply the requested changes to the image file
# History:
# Usage: called by edit.cgi
# Reference:
#    /mnt/public/www/cgi-bin/plex-log.cgi
# Improve:
# Documentation:
# Dependencies:
#    /etc/gallery-cgi.conf
#    sigal.conf.py from indicated gallery_id
DEBUG=0
export DRYRUN=
export meta="<meta" # for turning off the automatic reloads
test -e /etc/gallery-cgi.conf && . /etc/gallery-cgi.conf

response=0
printf "%s\n\n" "Content-type: text/html"
echo "<html><head><title>Saving changes...</title>"
echo "${meta} name=\"viewport\" content=\"width=device-width, initial-scale=1\">"
echo "</head><body>"
echo "<pre>"
if test "${REQUEST_METHOD}" != "POST" ;
then
   echo "Error! Unable to update title or description."
   response=1
   printf '%s' "</body></html>"
   exit ${response}
fi
export stdin="$( cat )"
# find separator
export boundary="$( echo "${CONTENT_TYPE}" | awk '{print $2}' | awk -F'=' '{print $2}' )"

# prepare the stdin
stdin2="$( echo "${stdin}" | sed -r -e "s/-*${boundary}-*/%%%%%%%%%%/g;" | awk '/%%%%%%%/{a=a+1} $0 !~ /%%%%%/ && a {print a,$0}' )"
# find metadatafile to write
export metadatafile="$( echo "${stdin2}" | awk '/Content-Disposition.*="metadatafile"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' | tr -d '[\n\r]' )"
# find title
export title="$( echo "${stdin2}" | awk '/Content-Disposition.*="title"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{$1="";print;}' | sed -r -e 's/^\s*//;' )"
# find description
#export description="$( echo "${stdin2}" | awk '/Content-Disposition.*="description"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{$1="";print;}' | sed -r -e 's/^\s*//;' )"
export description="$( echo "${stdin2}" | awk '/Content-Disposition.*="description"/{a=$1} $1==a && $0 !~ /Content-Disposition/{$1="";print;}' | sed -r -e 's/^\s*//;' )"
# find referer
export referer="$( echo "${stdin2}" | awk '/Content-Disposition.*="referer"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' )"
# find thumbnail
export thumbnail="$( echo "${stdin2}" | awk '/Content-Disposition.*="thumbnail"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' )"
# find author
export author="$( echo "${stdin2}" | awk '/Content-Disposition.*="author"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' )"
# find gallery_id from form data
export gallery_id="$( echo "${stdin2}" | awk '/Content-Disposition.*="id"/{a=$1} $1==a && $0 !~ /^[0-9]\s*$|Content-Disposition/{print $2}' | tr -d '[\r\n]' )"

eval export sigal_conf_py=\"\${"${gallery_id}"}\"/sigal.conf.py

# learn edit_enabled from config file
edit_enabled="$( awk -F'=' '$1 ~ /edit_enabled/ {print $2}' "${sigal_conf_py}" | sed -r -e "s/[\"']//g;" -e 's/^ //;' | tr '[A-Z]' '[a-z]' )"

test -n "${DEBUG}" && test ${DEBUG} -ge 3 1>/dev/null 2>&1 && {
   env
}

test -n "${DEBUG}" && test ${DEBUG} -ge 1 1>/dev/null 2>&1 && {
   test -n "${title}" && {
      echo "Title: ${title}"
      test -n "${thumbnail}" && echo "Thumbnail: ${thumbnail}"
      echo ""
   }
   echo "${description}"
}

{ test -z "${DRYRUN}" || test "${DRYRUN}" = "0" ; } && {
   if test "${edit_enabled}" = "false" ;
   then
      echo "Unable to edit."
      echo "${meta} http-equiv=\"Refresh\" content=\"1; url='${referer}'\" />"
   else
      echo "Will try to apply to file \"${metadatafile}\"!"
      {
         test -n "${title}" && echo "Title: ${title}"
         test -n "${thumbnail}" && echo "Thumbnail: ${thumbnail}"
         test -n "${author}" && echo "Author: ${author}"
         # the whitespace is not necessary because description always has plenty
         echo "${description}"
      } | tee "${metadatafile}" 1>/dev/null
      response=$?
      test "${response}" = "0" && {
         echo "Success. Redirecting..."
         echo "${meta} http-equiv=\"Refresh\" content=\"0.5; url='${referer}'\" />"
      }
   fi
}

printf '%s' "</body></html>"
exit ${response}
