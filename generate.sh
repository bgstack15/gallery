#!/bin/sh
# File: generate.sh
# Author: bgstack15@gmail.com
# Startdate: 2021-01-24
# Title: Generate symlink Forests 
# Package: gallery
# Purpose: turn indir into a year/month set of symlinks at outdir
# History:
# Usage:
#    Useful for running before sigal.bin.
# Reference:
# Improve:
#    replace tildes in video filenames!
#    rewrite in python
# Dependencies:
#    exiftool

test -z "${indir}" && export indir="${1}"
test -z "${outdir}" && export outdir="${2}"
{ test -z "${indir}" || test ! -d "${indir}" ; } && { echo "First parameter must be input path. Aborted." 1>&2 ; exit 1 ; }
{ test -z "${outdir}" || test ! -d "${outdir}" ; } && { echo "Second parameter must be output path. Aborted." 1>&2 ; exit 1 ; }
# strip trailing slashes
export indir="${indir%%/}"
export outdir="${outdir%%/}"
for tf in $( find "${indir}" ! -type d ! -name '*.sh' ! -name '*.git*' ! -name '.*.swp' ) ;
do
   YM="$( exiftool -t -createdate "${tf}" | awk -F'\t' '{print $NF}' | awk -F' ' '{print $1}' | awk -F ':' 'BEGIN{OFS="/"} {print $1,$2}' )"
   test ! -d "${outdir}/${YM}" && {
      test -n "${DEBUG}" && echo "mkdir -p ${outdir}/${YM}"
      test -z "${DRYRUN}" && mkdir -p "${outdir}/${YM}"
   }
   test -n "${DEBUG}" && echo "ln -s ${tf} ${outdir}/${YM}/"
   test -z "${DRYRUN}" && ln -s "${tf}" "${outdir}/${YM}/"
done
