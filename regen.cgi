#!/bin/sh
# File: /mnt/public/www/cgi-bin/gallery/regen.cgi
# Author: bgstack15@gmail.com
# Startdate: 2021-01-24
# SPDX-License-Identifier: GPL-3.0
# Title: Regenerate static pages
# Package: gallery
# Purpose: run sigal.bin on web request
# History:
# Usage: visit this to run the static site generator
# Reference:
#    ip-address.cgi
# Improve:
# Documentation:
# Dependencies:
#    sudoers rules from 60_regen_gallery_sudo
#    selinux rules from gallery.te
#    sigal.bin
# Reverse dependencies:
#    custom sigal theme
DEBUG=0
export meta="<meta" # for turning off the automatic reloads

printf "%s\n\n" "Content-type: text/html"
echo "<html><head><title>Regenerating...</title>"
echo "</head><body>"
test -n "${DEBUG}" && test "${DEBUG}" -ge 1 2>/dev/null && {
   echo "<pre>"
   env
   echo "</pre>"
}

# find gallery_id from query string
echo "${QUERY_STRING}" | grep -qE "(^|\&|\?)id=[^\&]+" && {
   export gallery_id="$( echo "${QUERY_STRING}" | grep -oE "(^|\&|\?)id=[^\&]+" | awk -F'=' '{$1="";print;}' | sed -r -e 's:^ ::' )"
}

sudo /usr/local/bin/sigal.bin "${gallery_id}"
result=$?
test $result -eq 0 && {
   echo "Success! Redirecting..."
   echo "${meta} http-equiv=\"Refresh\" content=\"1; url='${HTTP_REFERER}'\" />"
}
printf '%s' "</body></html>"
