# README for Gallery Solution for example
// File: /mnt/public/Support/Programs/gallery/gallery.md
// Location: server1:/var/server1/shares/public
// Author: bgstack15@gmail.com
// Startdate: 2021-01-26 17:05
// Title: Readme for Media Gallery solution for Example
// Purpose: Document the system and configuration
// History:
// Usage:
// Reference:
// Improve:
//    document generate.py from d2-03a:~/dev/gallery/

## Overview
As part of my goals to run self-hosted services for myself and my family, I intend to maintain a web gallery of photos and videos.
This solution consists of several parts.
* Tools that create symlink forests to the original image files, in a specific location.
* Static site generator [sigal](http://sigal.saimon.org/)
* Customized theme for sigal
* Customized sigal config for that theme
* Gallery id table
* SELinux rules
* CGI scripts for Apache httpd

## Architecture
The solution runs on server1, the main file server for the internal network.
### Tools that create symlink forests for the gallery source directories
`Generate.sh` is what I used for the proof of concept. This needs to be rewritten in python, and to handle files without exif data.
See python project "generate" (FIXME) for the improved version.

### Static site generator
The [sigal](https://sigal.saimon.org/) site generator is installed with pip3, under local service account `sigal` on server1. A special script, `/usr/local/bin/sigal.bin` will generate the static pages for a site. TO use this script, pass a parameter of the source directory where the sigal.conf.py exists.

    /usr/local/bin/sigal.bin /mnt/public/www/example/images/.gallery

Sigal depends on ``ffmpeg` for video thumbnailing and conversions. On CentOS 8, ffmpeg is in repository powertools.

### Customized theme for sigal
Use theme `bgstack15-gallery-theme`, in any location. You just need to put its full path as the value of the theme in a sigal.conf.py for a gallery.
This is a fork of the default, included [colorbox](https://github.com/saimn/sigal/tree/master/sigal/themes/colorbox) theme. The full theme is available in this project directory, as well as a diff of it to the original as of sigal version 2.1.1.
The main changes are adding extra metadata handlers and link logic for the custom links related to editing metadata.

### Customized sigal config for that theme
When you run `sigal init` in a directory, it generates a default config you can modify. The example theme, and this whole solution, depends on adding a number of settings. A summary of the specific options is here, but the full example file is in this project directory.

    source = '/var/www/gallery/.my2018'
    destination = '/var/www/gallery/my2018'
    use_orig = True
    edit_cgi_script = "/cgi-bin/gallery/edit.cgi" # web path to edit.cgi
    edit_enabled = False
    edit_password = "makeupapassword"
    edit_string = '[edit]' # text of link to edit metadata
    toggle_enable_string = "Enable editing"
    toggle_disable_string = "Disable editing"
    toggle_link = "/cgi-bin/gallery/toggle-editing.cgi" # web path to toggle-editing.cgi
    toggle_editing = [
            (False, 'Enable editing'),
            (True, 'Disable editing')
        ]

    gallery_id = "example_images"

    # A list of links (tuples (title, URL))
    # links = [('Example link', 'http://example.org'),
    #          ('Another link', 'http://example.org')]
    links = [
        ('Regenerate pages', '/cgi-bin/gallery/regen.cgi?id=' + gallery_id)
            ]

The `gallery_id` is very important, because the cgi scripts and theme rely on it.

### Gallery id table
File `/etc/gallery-cgi.conf` contains a list of `gallery_id` translations to directories with sigal.conf.py rules.

    example_images=/mnt/public/www/example/images/.gallery

### SELinux rules
The reference system, server1, runs SELinux. A custom selinux module is needed to allow all the operations that are a part of this gallery solution, which include the following.

File `gallery.te` can be installed as an enabled selinux module.

    sudo checkmodule -M -m -o gallery.mod gallery.te && sudo semodule_package -o gallery.pp -m gallery.mod && sudo semodule -i gallery.pp

### Sudo rules
For apache httpd to be able to run the sigal.bin, set up sudoers rules. File `60_regen_galery_sudo` adds the permission necessary.

### CGI scripts for Apache httpd
The main focus of the gallery project is the ability to edit metadata from the web view. While sigal is great for developers, some users might only care about editing metadata from where they are actually viewing the media.
* edit.cgi is called from the custom theme's "edit" links, and includes the form for making changes to media metadata.
* apply.cgi actually makes the changes, and is called from the edit.cgi form.
* regen.cgi invokes sigal.bin which re-runs sigal.
* toggle-editing.cgi enables or disables editing. Enabling requires a password.
These can be placed anywhere you have enabled CGI for httpd, but the canonical location is `/var/www/cgi-bin/gallery/`.

## Operations
I anticipate that more work is needed on an ongoing basis. Here are some processes that can be used.

### Make a new gallery
To establish a new gallery, change directory to the source directory for the gallery and run command

    sigal.bin init

Which generates the basic sigal.conf.py. Add the pertinent variables, described in section "Customized sigal config for that theme" above.

### Run sigal from command line
While the web links for "regen.cgi" are great for when you are viewing the web, you can also run the sigal.bin from the cli. You need to include a path to the directory that holds a sigal.conf.py, or else a `gallery_id` from /etc/gallery-cgi.conf.

    sigal.bin example_images

### Make metadata changes directly on filesystem
You can of course, as designed by the author of sigal, go edit any **${IMAGENAME%%.jpg}.md** file with the relevant fields. See references [1][r1] and [2][r2] for the available fields.
File index.md will be the metadata for the directory itself.

## History
In 2020, I installed [piwigo](https://piwigo.org/) on a dev system. I didn't want to deal with php, so I dropped it. In January 2021, I started listing various options for self-hosted galleries. Read heading [Related Files] for those.
Criteria I assembled includes
* Metadata: description, date, comments

## Related Files
These files are important to this gallery project.
* /usr/local/bin/sigal.bin
* /var/www/cgi-bin/apply.cgi
* /var/www/cgi-bin/edit.cgi
* /var/www/cgi-bin/regen.cgi
* /var/www/cgi-bin/toggle-editing.cgi
* gallery.te
* /etc/sudoers.d/60_regen_gallery_sudo
* /etc/gallery-cgi.conf
* sigal.conf.py
* example-theme/

## Alternatives
* [Piwigo](https://piwigo.org/) uses PHP which I don't want to maintain.
* [Awesome selfhosted](https://github.com/awesome-selfhosted/awesome-selfhosted#photo-and-video-galleries) is a whole list.
Ones I considered without trying
* [mediagoblin](http://mediagoblin.org/)
* [photoview](https://github.com/viktorstrate/photoview)
* [pigallery2](https://bpatrik.github.io/pigallery2/)
Ones I listed as tolerable, but not focused on what I need.
* [lychee](https://lycheeorg.github.io/)
* [photato](https://github.com/trebonius0/Photato)
* [PhotoFloat](http://blog.zx2c4.com/567) written by the cgit author. He has a large body of good works.

## References
### Weblinks
[w1]: http://sigal.saimon.org/en/latest/album_information.html
[w2]: http://sigal.saimon.org/en/latest/image_information.html
[w3]: https://thealaskalinuxuser.wordpress.com/2019/07/23/home-photo-server-part-2-apache-phpalbum-and-piwigo/ Thealaskalinuxuser's guide to a home photo server with piwigo
